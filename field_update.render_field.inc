<?php
/**
 * @file Render field content of a single field.
 */

/**
 * Returns the rendered field or false
 * @param $entity_type
 * @param $entity_id
 * @param $field_name
 * @param string $display
 * @param last_update
 * @param force_update
 */
function field_update_render_field($entity_type, $entity_id, $field_name, $display = 'default', $last_update = 0, $force_update = FALSE) {

  // basic return object
  $ret = new stdClass();
  $ret->field_name = $field_name;
  $ret->entity_id = $entity_id;
  $ret->entity_type = $entity_type;
  $ret->view_mode = $display;
  $ret->field = FALSE;
  $ret->error = TRUE;
  $ret->updated = FALSE;
  $ret->access = FALSE;

  // load entity
  $entities = entity_load($entity_type, array($entity_id));

  // if we found the entity
  if (isset($entities[$entity_id])) {

    // get entity
    $entity = $entities[$entity_id];

    // check if user can view this entity
    if (entity_access('view', $entity_type, $entity)) {

      $ret->access = TRUE;

      // if this entity was updated AFTER the last time we checked
      if ($entity->changed > $last_update || $force_update) {

        // render field
        $field = field_view_field($entity_type, $entity, $field_name, $display);
        $ret->field = render($field);

        // mark this field as updated
        $ret->updated = TRUE;
      }

      $ret->error = FALSE;
    }

    // if now allowed, set HTTP status header
    else {
      drupal_add_http_header('Status', 403);
    }

  // if we haven't found the entity
  } else {
    drupal_add_http_header('Status', 404);
  }

  return $ret;
}