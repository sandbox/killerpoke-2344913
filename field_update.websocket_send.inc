<?php
/**
 * @file WebSocket send to client.
 */

$module_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'field_update');
require_once  $module_path . '/field_update.render_field.inc';

// Include DnodeSyncClient to communicate with the node.js WebSocket server
require_once(__DIR__ . '/vendor/DnodeSyncClient.php');

function field_update_websocket_send($entity_type, $field_name, $entity_id) {

  // we need to get all view modes and send updates for all viewmodes, since we don't know which viewmodes are in use.
  $entity_info = entity_get_info($entity_type);
  $last_update = 0;
  $view_modes = array('default', 'full');
  $port = variable_get('field_update_websockets_server_port');
  $endpoint = variable_get('field_update_websockets_endpoint');

  /**
   * get all view modes for this entity and send updates to all view modes.
   * We need to do this for all view modes, since we don't know which client
   * registered for which view mode.
   */
  foreach ($entity_info['view modes'] as $key => $info) {
    if ($info['custom settings']) {
      $view_modes[] = $key;
    }
  }

  // for all view modes
  foreach ($view_modes as $view_mode) {

    // get a field_update object, containing field data and the rendered field.
    $ret = field_update_render_field($entity_type, $entity_id, $field_name, $view_mode, $last_update);

    // generete the field_id
    $update_id = $ret->entity_type . '-' . $ret->entity_id . '-' . $ret->field_name . '-' . $ret->view_mode;

    try {

      // Connect to DNode server
      $dnode = new \DnodeSyncClient\Dnode();

      $connection = $dnode->connect($endpoint, $port);

      // call fieldUpdate on the node.js DNode server.
      $response = $connection->call('fieldUpdate', array($update_id, drupal_json_encode($ret)));

      // if there was an error, print it
      if (!$response[1]) {
        drupal_set_message(t('Could not send updates to WebSocket Server on: "' . $endpoint . ':' . $port . '"!'), 'warning');
      }

    } catch(Exception $e) {
      drupal_set_message(t('Could not connect to WebSocket Server. No updates where send to the client.'), 'warning');
    }
  }
}