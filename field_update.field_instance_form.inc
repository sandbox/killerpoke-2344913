<?php
/**
 * @file alter field instance form to enabled or disable field_update.
 */

/**
 *  Implements hook_form_field_ui_field_edit_form_alter().
 */
function field_update_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {

  // fieldset for all field_update config fields
  $form['instance']['settings']['field_update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field Update'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // enable field update for this field or not
  $form['instance']['settings']['field_update']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Field Update for this field'),
    '#default_value' => isset($form['#instance']['settings']['field_update']['enabled']) ? $form['#instance']['settings']['field_update']['enabled'] : FALSE,
  );

  // set ajax fallback pull time
  $form['instance']['settings']['field_update']['pull_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Ajax Pull time in seconds'),
    '#description' => t('When a browser don\'t support WebSockets, field updates get pulled every X seconds via ajax.'),
    '#default_value' =>  isset($form['#instance']['settings']['field_update']['pull_time']) ? $form['#instance']['settings']['field_update']['pull_time'] : 5,
    '#element_validate' => array('field_update_validate_pull_time'),
    '#states' => array(
      'visible' => array(
        'input[name="instance[settings][field_update][enabled]"]' => array(
          'checked' => TRUE
        ),
      ),
    ),
  );
}

/**
 * Validation callback for ajax pull time field.
 */
function field_update_validate_pull_time($element, &$form_state, &$form) {
  if (isset($element['#value'])) {
    if (!is_numeric($element['#value']) || $element['#value'] <= 0) {
      form_error($element, t('Ajax pull time must be an integer value, greater then 0.'));
    }
  }
}