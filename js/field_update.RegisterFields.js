/**
 * @file Register fields for field update.
 */
(function ($) {

  Drupal.FieldUpdate = {};
  Drupal.behaviors.FieldUpdate = {};

  /**
   * Attaches outline behavior for regions associated with contextual links.
   */
  Drupal.behaviors.FieldUpdate = {

    /**
     * Holds all fields, where field update is enabled
     */
    fields: [],

    createFieldId: function(data) {
      return data.entity_type + '-' + data.entity_id + '-' + data.field_name + '-' + data.view_mode;
    },

    attach: function (context) {

      var t = this;

      // for each element with a not empty data-field-update attribute
      $('[data-field-update]', context).each(function(){

        var data = $(this).data('field-update');
        var id = t.createFieldId(data);

        // add this field to the fields list
        t.fields.push({
          dom: $(this),
          data: data,
          id: id
        });

      });

      // if we have at least on field update field on the page we need to initialize FieldUpdate
      t.initFieldUpdate();
    },

    /**
     * Initialize field update.
     * Connect to WebSocketServer or set mode to Ajax
     */
    initFieldUpdate: function() {

      var t = this;
      var useAjax = true;

      if(t.fields.length > 0) {

        // If browser supports websockets && websocket is enabled for field update
        if ("WebSocket" in window && Drupal.settings.field_update.WebSocketEnabled) {

          useAjax = false;

          // try to connect to WebSocketServer
          var endpoint = Drupal.settings.field_update.WebSocketEndpoint;
          var port = Drupal.settings.field_update.WebSocketPort;

          // prepare query
          var query = 'field_ids=';

          $.each(t.fields, function(key, field){
            query += field.id + ',';
          });

          query = query.substr(0, query.length - 1);

          // create a socket io object and connect to server
          var socket = io('http://' + endpoint + ':' + port, { reconnection: false, query: query });

          // try to connect to websocket
          socket.on('connect', function(){
            console.log('Connected!');
          });

          socket.on('connect_error', function(){

            // on connection error fallback to ajax
            $.each(t.fields, function(key, field) {
              t.ajaxHandler(field);
            });

          });

          // debug on disconnect
          socket.on('disconnect', function(){
            console.log('Disconnected');

            // on disconnection fallback to ajax
            $.each(t.fields, function(key, field) {
              t.ajaxHandler(field);
            });
          });

          // when we get an update from the server, update the field
          socket.on('update', function(response){
            t.updateCallback(response);
          });

        }

        // register for ajax updates
        if(useAjax) {

          $.each(t.fields, function(key, field) {
            t.ajaxHandler(field);
          });

        }
      }
    },

    updateCallback: function(data) {

      // if we want to update the field
      if(data.updated && !data.error && data.field) {

        var t = this;

        // find dom obj
        $.each(t.fields, function (key, field) {

          if (field.data.entity_type == data.entity_type) {
            if (field.data.entity_id == data.entity_id) {
              if (field.data.field_name == data.field_name) {
                if (field.data.view_mode == data.view_mode) {

                  // replace field with new value from server
                  dom = field.dom;
                  var new_obj = $(data.field);
                  new_obj.insertAfter(field.dom);
                  t.fields[key].dom.remove();
                  t.fields[key].dom = new_obj;

                }
              }
            }
          }
        });
      }
    },

    ajaxHandler: function(field) {

      var t = this;
      var data = field.data;
      var obj = field.dom;

      // wait the pull time...
      setTimeout(function(){

        // create url to get updates
        var url = Drupal.settings.basePath + Drupal.settings.field_update.AjaxEndpoint;
        url = url + '/' + data.entity_type + '/' + data.field_name + '/' + data.bundle;
        url = url + '/' + data.entity_id + '/' + data.view_mode + '/' + data.last_update;

        // get updates from server
        $.getJSON(url, function(response){

          t.updateCallback(response);

          // on response call this handler again
          t.ajaxHandler(field);

        });

        // Set the Pull time from field config.
      }, data.pull_time * 1000);

    }

  };


})(jQuery);