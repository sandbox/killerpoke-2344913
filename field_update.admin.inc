<?php
/**
 * @file admin settings for field_update
 */

function field_update_admin() {

  $form = array();

  // enable websockets
  $form['field_update_websockets'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Websockets for field_update. As a fallback, field_update will use ajax pulls.'),
    '#default_value' => variable_get('field_update_websockets', FALSE),
  );

  // set websockets endpoint
  $form['field_update_websockets_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Websockets Endpoint'),
    '#description' => t('Set endpoint, where the websocket server is located (e.g.:"localhost").'),
    '#default_value' => variable_get('field_update_websockets_endpoint', ""),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        'input[name="field_update_websockets"]' => array(
          'checked' => TRUE
        ),
      ),
    ),
  );

  // set websockets server port
  $form['field_update_websockets_server_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Websockets Server Port'),
    '#description' => t('This port is used by zmq to send updates to the Server (e.g.:"5555").'),
    '#default_value' => variable_get('field_update_websockets_server_port', ""),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        'input[name="field_update_websockets"]' => array(
          'checked' => TRUE
        ),
      ),
    ),
  );

  // set websockets client port
  $form['field_update_websockets_client_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Websockets Client Port'),
    '#description' => t('This port is used by the client (Browser) to register on the server for updates (e.g.:"8080")'),
    '#default_value' => variable_get('field_update_websockets_client_port', ""),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        'input[name="field_update_websockets"]' => array(
          'checked' => TRUE
        ),
      ),
    ),
  );

  return system_settings_form($form);

}