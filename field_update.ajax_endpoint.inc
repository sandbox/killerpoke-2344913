<?php
/**
 * @file ajax endpoint for field update.
 */

$module_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'field_update');
require_once  $module_path . '/field_update.render_field.inc';

function field_update_ajax_endpoint($entity_type, $field_name, $bundle, $entity_id, $view_mode, $last_update) {

  // get instance from params
  $instance = field_info_instance($entity_type, $field_name, $bundle);

  // if field_update is enabled for this field
  if (isset($instance['settings']['field_update']['enabled'])) {
    if ($instance['settings']['field_update']['enabled']) {

      // return rendered field
      drupal_json_output(field_update_render_field($entity_type, $entity_id, $field_name, $view_mode, $last_update));
      return;
    }
  } else {

    // if field update was not enabled, then there is no endpoint for this field
    drupal_add_http_header('Status', 404);
  }

  drupal_exit();
  return;
}