/**
 * We are using socket.io to send updates to the client and dnonde to receve udpates from our
 * Drupal installation. Because of this, there must be a public port for communicating with
 * the clients and a local port to get infos from Drupal.
 */
var socket_io_port = 8080;
var dnode_port = 5555;

var io = require('socket.io')();
var dnode = require('dnode');

/**
 * The list of all connected clients, we save them here, so we can send updates to them.
 * @type {Array}
 */
var clients = []; //this array will hold all connected clients, keyed by fields

/**
 * Socket.io connection callback.
 * Register clients for a specific field (query.field_id) to get updates.
 */
io.on('connection', function(socket){

  // if the field_id query parameter is set
  if(socket.handshake.query.field_ids) {

    var field_ids = socket.handshake.query.field_ids;

    var client_fields = field_ids.split(',');

    client_fields.forEach(function(field){

      if(clients[field]) {
        clients[field].push(socket);
      } else {
        clients[field] = [socket];
      }

      // debug output
      console.log('client con. on: ' + field + '. New clients[field].length: ' + clients[field].length);

    });

    /**
     * Socket.io disconnect callback.
     * Delete client from the client list, on disconnection.
     */
    socket.on('disconnect', function(){

      // remove client from client list
      client_fields.forEach(function(field) {

        if(clients[field]) {
          var i = clients[field].indexOf(socket);
          clients[field].splice(i, 1);
        }

        // debug output
        console.log('client disconnected on field: ' + field);

      });
    });
  }
});

/**
 * Socket.io listens on this port. You cen set the port at the beginning of this file.
 */
io.listen(socket_io_port);

/**
 * Starting the dnode server.
 * This server listens for dnode messages from Drupal. When a message is received, we need
 * to send the update to all clients.
 */
var dnode_server = dnode({

  /**
   * fieldUpdate.
   * This function get's called by Drupal via a dnode message to update all clients, that
   * registered for [field_id] updates.
   * @param field_id, field_id of the field to get updates from
   * @param data, the Drupal data object
   * @param cb, callback function that get's called after updating the clients.
   */
  fieldUpdate: function(field_id, data, cb){

    console.log(field_id);

    // if the field_id is avaiable in the client array
    if(clients[field_id]) {

      // send updates to all clients
      for (i = 0; i < clients[field_id].length; i++) {
        clients[field_id][i].emit('update', JSON.parse(data));
      }

      // debug output
      console.log('sending updates to clients for field: ' + field_id);
    }

    // call Drupal callback.
    cb(null, true);
  }
});

/**
 * Dnode listens on this port. You cen set the port at the beginning of this file.
 */
dnode_server.listen(dnode_port);