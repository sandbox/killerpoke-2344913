<?php
/**
 * @file Testing the admin configuration page.
 */

class FieldUpdateFieldInstanceFormTest extends DrupalWebTestCase {

  protected $editor_user;
  protected $anon_user;

  public static function getInfo() {
    return array(
      'name' => 'Field Update field instance form test',
      'description' => 'Testing the field instance form configuration page.',
      'group' => 'FieldUpdate',
    );
  }

  protected function setUp() {
    parent::setUp('field_update');
    $this->editor_user = $this->drupalCreateUser(array('administer content types'));
    $this->anon_user = $this->drupalCreateUser(array());
  }

  /**
   * Test rendering of field update fields on field instance forms
   */
  function testFieldUpdateFieldInstanceForm() {

    // try to access the admin page without proper permissions (administer field_update settings)
    $this->drupalLogin($this->editor_user);
    $this->drupalGet('admin/structure/types/manage/article/fields/body');
    $this->assertResponse(200);
    $this->assertText('Enable Field Update for this field');
    $this->assertText('Ajax Pull time in seconds');

    // try to send update settings, but with wrong pull time format
    $data = array(
      'instance[settings][field_update][enabled]' => TRUE,
      'instance[settings][field_update][pull_time]' => 'this is no number',
    );

    $this->drupalPost(
      'admin/structure/types/manage/article/fields/body',
      $data,
      'Save settings'
    );

    $this->assertResponse(200);
    $this->assertText('Ajax pull time must be an integer value, greater then 0.');

    // tryp to send valid data
    $data = array(
      'instance[settings][field_update][enabled]' => TRUE,
      'instance[settings][field_update][pull_time]' => rand(0, 100),
    );

    $this->drupalPost(
      'admin/structure/types/manage/article/fields/body',
      $data,
      'Save settings'
    );

    $this->assertResponse(200);
    $this->assertText('Saved Body configuration.');

    // get field instance settings and compare them to $data
    $instance = field_info_instance('node', 'body', 'article');
    $this->assertEqual(
      $instance['settings']['field_update']['enabled'],
      $data['instance[settings][field_update][enabled]']
    );

    $this->assertEqual(
      $instance['settings']['field_update']['pull_time'],
      $data['instance[settings][field_update][pull_time]']
    );
  }
}