<?php
/**
 * @file Testing inserting field update data as dom data to fields
 */

class FieldPreProcessFieldTest extends DrupalWebTestCase {

  protected $editor_user;

  public static function getInfo() {
    return array(
      'name' => 'Field Update process field',
      'description' => 'Testing inserting field update data as dom data to fields.',
      'group' => 'FieldUpdate',
    );
  }

  protected function setUp() {
    parent::setUp('field_update');
    $this->editor_user = $this->drupalCreateUser(array('administer content types', 'create article content'));
  }

  /**
   * Check if field update data gets inserted as dom data to fields
   */
  function testDOMData() {

    $langcode = LANGUAGE_NONE;

    // test node
    $node_data = array(
      'title' => $this->randomName(),
      "body[$langcode][0][value]" => $this->randomName(),
    );

    //login
    $this->drupalLogin($this->editor_user);

    // enable field settings for article body
    $data = array(
      'instance[settings][field_update][enabled]' => TRUE,
      'instance[settings][field_update][pull_time]' => rand(0, 100),
    );

    $this->drupalPost(
      'admin/structure/types/manage/article/fields/body',
      $data,
      'Save settings'
    );

    $this->assertResponse(200);
    $this->assertText('Saved Body configuration.');

    // create the node
    $this->drupalPost('node/add/article', $node_data, t('Save'));
    $this->assertResponse(200);
    $this->assertText('Article ' . $node_data['title'] . ' has been created.');

    // get node
    $node = $this->drupalGetNodeByTitle($node_data['title']);

    // you shouldn't need any special roles to see the effect
    $this->drupalLogout();

    // navigate to node
    $this->drupalGet('node/' . $node->nid);
    $this->assertResponse(200);

    // check if field update inserted the right dom data
    $body_fields = $this->xpath("//div[contains(@class, 'field-name-body')]");
    $body_field_attributes = $body_fields[0]->attributes();
    $dom_data = $body_field_attributes['data-field-update'];

    // check if dom data where pe injected into the field
    $this->assertNotNull($dom_data);

    // check if the attributes are as expected
    $dom_obj = json_decode($dom_data);

    $this->assertEqual($dom_obj->enabled, TRUE);
    $this->assertEqual($dom_obj->view_mode, 'full');
    $this->assertEqual($dom_obj->entity_type, 'node');
    $this->assertEqual($dom_obj->bundle, 'article');
    $this->assertEqual($dom_obj->field_name, 'body');
    $this->assertEqual($dom_obj->entity_id, $node->nid);
    $this->assertEqual($dom_obj->pull_time, $data['instance[settings][field_update][pull_time]']);

    // we don't know the exact time, but it must be bigger or equal then node create
    $this->assertTrue(($dom_obj->last_update >= $node->created));
  }
}